package com.dev.angel.utils;

import org.hibernate.HibernateError;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {


    private static SessionFactory sessionFactory;

    public  static ThreadLocal<Session> sessionThreadLocal = new ThreadLocal<>();


    static {
        try
        {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        }catch (HibernateException execption)
        {
            System.out.println("Problem creating session factory");
            execption.printStackTrace();
        }
    }

    public static Session currentSession() throws HibernateException {
        Session session = (Session) sessionThreadLocal.get();

        if(session == null){
            session = sessionFactory.openSession();
            sessionThreadLocal.set(session);
        }

        return session;
    }

    public  static void closeSession() throws HibernateException
    {
        Session session = (Session) sessionThreadLocal.get();
        sessionThreadLocal.set(null);

        if(session != null)
            session.close();

    }


}

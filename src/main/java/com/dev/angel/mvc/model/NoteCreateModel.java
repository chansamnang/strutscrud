package com.dev.angel.mvc.model;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;

public class NoteCreateModel extends ActionForm {

    private String  tittle;
    private String body;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;


    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void reset (ActionMapping mapping, HttpServletRequest request) {

        this.tittle = "";
        this.body = "";
        this.id = null;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if(tittle.length() == 0){
            errors.add("title.empty", new ActionMessage("error.required"));
        }

        if(body.length() == 0){
            errors.add("body.empty", new ActionMessage("error.required"));
        }
        return errors;
    }


}

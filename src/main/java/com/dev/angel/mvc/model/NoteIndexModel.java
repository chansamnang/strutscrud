package com.dev.angel.mvc.model;

import com.dev.angel.domain.entity.Note;
import org.apache.struts.action.ActionForm;


import java.util.List;

public class NoteIndexModel extends ActionForm {

    private List<Note> notes;


    public void setNotes(List<Note> list)
    {
        this.notes = list;
    }

    public List<Note> getNotes() {
        return this.notes;
    }


}

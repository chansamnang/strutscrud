package com.dev.angel.mvc.controller.note;

import com.dev.angel.domain.entity.Note;
import com.dev.angel.mvc.model.NoteCreateModel;
import com.dev.angel.repository.NoteRepository;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoteEditAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        NoteCreateModel model = (NoteCreateModel) form;

        NoteRepository repository = new NoteRepository();
        Integer id = Integer.parseInt(request.getParameter("id"));
        try
        {
            Note note =  repository.findOne(id);
            model.setBody(note.getBody());
            model.setTittle(note.getTitle());
            model.setId(note.getId());
        }catch (Exception ex)
        {
           return mapping.findForward("errors");
        }

        return mapping.findForward("success");
    }

}

package com.dev.angel.mvc.controller.note;

import com.dev.angel.domain.entity.Note;
import com.dev.angel.mvc.model.NoteCreateModel;
import com.dev.angel.repository.NoteRepository;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

public class NoteSaveAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        NoteCreateModel model = (NoteCreateModel) form;

        Note note = new Note();
        if(model.getId() != null && model.getId() != 0) {
            note.setId(model.getId());
        }
        note.setTitle(model.getTittle());
        note.setBody(model.getBody());
        note.setCreated(new Date());

        NoteRepository repository = new NoteRepository();
        repository.save(note);

        return mapping.findForward("success");
    }
}

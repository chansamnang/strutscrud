package com.dev.angel.mvc.controller.note;

import com.dev.angel.action.form.HomeForm;
import com.dev.angel.domain.entity.Note;
import com.dev.angel.mvc.model.NoteIndexModel;
import com.dev.angel.repository.NoteRepository;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class NoteIndexAction extends Action {


    @Override
    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response)  throws Exception {

        NoteRepository repository = new NoteRepository();
        List<Note> list = repository.getAll();

        NoteIndexModel model = (NoteIndexModel) form;
        model.setNotes(list);

//       request.setAttribute("list", list);

        return mapping.findForward("note_index");

    }


}

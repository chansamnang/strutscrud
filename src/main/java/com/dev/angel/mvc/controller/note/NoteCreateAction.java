package com.dev.angel.mvc.controller.note;

import com.dev.angel.mvc.model.NoteCreateModel;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoteCreateAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response)  throws Exception {

        NoteCreateModel model = (NoteCreateModel)form;

        return mapping.findForward("success");
    }





}

package com.dev.angel.mvc.controller.note;

import com.dev.angel.mvc.model.NoteCreateModel;
import com.dev.angel.repository.NoteRepository;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoteDeleteAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        NoteRepository repository = new NoteRepository();
        Integer idToDelete =  Integer.parseInt(request.getParameter("id"));
        try {
            repository.remove(idToDelete);
        }catch (Exception ex)
        {
            ex.printStackTrace();
            return mapping.findForward("error");
        }

        return mapping.findForward("success");
    }

}

package com.dev.angel.action.form;


import org.apache.struts.action.ActionForm;

public class HomeForm extends ActionForm
{
    String message;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
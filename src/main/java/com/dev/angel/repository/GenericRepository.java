package com.dev.angel.repository;

import com.dev.angel.utils.HibernateUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericRepository<TEntity, TId extends  Number> implements Repository<TEntity, TId> {

    @Override
    public List<TEntity> getAll() {

        List<TEntity> list = null;

        try {

            Session session = HibernateUtils.currentSession();
            Query query = session.createQuery("from " + getClassType().getName());
            list = query.list();

        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

        if(list == null)
            list = new ArrayList<>();
        return list;
    }

    @Override
    public TEntity findOne(TId id) {

        TEntity entity = null;

        try {
            Session session = HibernateUtils.currentSession();
            entity = (TEntity) session.get(getClassType(), id);
            return entity;

        }catch (Exception e)
        {
            throw new HibernateException("Select all error");
        }finally {
            HibernateUtils.closeSession();
        }
    }

    @Override
    public TEntity save(TEntity entity) {

        Session session = HibernateUtils.currentSession();
        Transaction transaction = null;
        boolean rollback = true;

        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(entity);
            transaction.commit();
            rollback = false;
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally {
            if(rollback && transaction != null)
            {
                transaction.rollback();
            }
            HibernateUtils.closeSession();
        }
        return null;
    }

    @Override
    public void remove(TId id) {

        Session session = HibernateUtils.currentSession();
        Transaction transaction = null;
        boolean rollback = true;

        try {
            transaction = session.beginTransaction();
            TEntity entity = (TEntity)session.get(getClassType(), id);
            session.delete(entity);
            transaction.commit();
            rollback = false;
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally {
            if(rollback && transaction != null)
            {
                transaction.rollback();
            }
            HibernateUtils.closeSession();
        }
    }


    abstract Class<TEntity> getClassType();
}

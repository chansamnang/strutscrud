package com.dev.angel.repository;

import java.util.List;

public interface Repository<TEntity, V extends Number> {

    List<TEntity> getAll();
    TEntity findOne(V id);
    TEntity save(TEntity entity);
    void remove(V id);
}

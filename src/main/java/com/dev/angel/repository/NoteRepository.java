package com.dev.angel.repository;

import com.dev.angel.domain.entity.Note;

public class NoteRepository extends GenericRepository<Note, Integer> {

    @Override
    Class<Note> getClassType() {
        return Note.class;
    }
}

package com.dev.angel.domain.entity;

import com.dev.angel.domain.BaseModel;

import java.util.Date;

public class Note extends BaseModel {

    private String title;
    private String body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreated() {
        return super.getCreated();
    }

    public void setCreated(Date created) {
        super.setCreated(created);
    }


}

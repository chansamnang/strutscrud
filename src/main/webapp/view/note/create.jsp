<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<html:form action="/note/save">
    <div>
        <html:hidden property="id"/>
    </div>
    <div>
        <h5>Title: </h5>
        <html:text property="tittle" />
        <small> <html:errors  property="title.empty" /></small>
    </div>
    <div>
        <h5 >Body:</h5>
        <html:textarea property="body"/>
        <small><html:errors property="body.empty" /></small>
    </div>
    <div>
        <html:submit value="Add"/>
    </div>
</html:form>
</body>
</html>

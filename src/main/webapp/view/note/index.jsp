
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<html>
<head>
    <title>Title</title>

    <style>
        .box {
            padding: 15px 15px;
            display: inline-block;
            width: 30%;
            margin-right: 3px;
            border: 1px solid rgba(127, 127, 127, 0.3);
        }

        .box .title {
            color:skyblue;
        }
        .box .body {
            padding: 5px 5px;
        }

        .box .date::before {
            content: "*";
            color: red;
        }
        .right {
            display: block;
            float: right;
        }

    </style>

</head>
<body>
<h2>My note</h2>
<ul>
    <li><a href="/api/note/create">Create</a></li>
</ul>
<hr>
<logic:iterate scope="request" name="noteIndex" property="notes" id="item">
    <div class="box">
        <h4 class="title"><bean:write name="item" property="title"/></h4>
        <p class="body">
            <bean:write name="item" property="body"/>
        </p>
        <span class="date" ><fmt:formatDate value="${item.created}" type="date" dateStyle="long" /></span>
        <span class="right">
            <a href="/api/note/delete?id=<bean:write name="item" property="id"/>">delete</a>
            <a href="/api/note/edit?id=<bean:write name="item" property="id"/>">edit</a>
        </span>
    </div>
</logic:iterate>

</body>
</html>
